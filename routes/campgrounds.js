var express = require('express');
var router = express.Router();
var Campground = require('../models/campground');
var middleware = require('../middleware');
var Review = require('../models/review');
var Comment = require('../models/comment');
var decode = require('decode-html');

/* -------- INDEX --------> show all campgrounds */

router.get('/', function(req, res) {
	//get all the campgrounds from the db
	Campground.find({}, function(err, allCampgrounds) {
		if (err) {
			console.log(err);
		} else {
			res.render('campgrounds/index', { campgrounds: allCampgrounds });
		}
	});
});

/* -------- Create --------> add new campgrounds to DB */

router.post('/', middleware.isLoggedIn, function(req, res) {
	var name = req.body.name;
	var price = req.body.price;
	var image = req.body.image;
	var dsc = req.body.description;
	var author = {
		id: req.user._id,
		username: req.user.username
	};
	var newCampground = { name: name, price: price, image: image, description: dsc, author: author };
	// Create a new campgrounds and save to the database
	Campground.create(newCampground, function(err, newlyCreated) {
		if (err) {
			console.log(err);
		} else {
			//redirect back to campgrounds page
			console.log(newlyCreated);
			res.redirect('/campgrounds');
		}
	});

	// var fullName = req.body.fullName;
	// var Age = req.body.Age;
	// var company = req.body.company;
	// var country = req.body.country;
	// var city = req.body.city;
	// console.log(fullName);

	// var newTable = { fullname: fullName, Age: Age, company: company, country: country, city: city };

	// Table.create(newTable, function(err, newlyCreatedTable) {
	// 	if (err) {
	// 		console.log(err);
	// 	} else {
	// 		console.log(newlyCreatedTable);
	// 		res.redirect('/campgrounds');
	// 	}
	// });
});

/* -------- NEW --------> show form to create new campground */
router.get('/new', middleware.isLoggedIn, function(req, res) {
	res.render('campgrounds/new');
});

// SHOW - shows more info about one campground
router.get('/:id', function(req, res) {
	//find the campground with provided ID
	Campground.findById(req.params.id)
		.populate('comments')
		.populate({
			path: 'reviews',
			options: { sort: { createdAt: -1 } }
		})
		.exec(function(err, foundCampground) {
			if (err) {
				console.log(err);
			} else {
				//render show template with that campground
				res.render('campgrounds/show', middleware.checkCampgroundOwnership, { campground: foundCampground });
			}
		});
});

// // Campground Like Route
// router.post('/:id/like', middleware.isLoggedIn, function(req, res) {
// 	Campground.findById(req.params.id, function(err, foundCampground) {
// 		if (err) {
// 			console.log(err);
// 			return res.redirect('/campgrounds');
// 		}

// 		// check if req.user._id exists in foundCampground.likes
// 		var foundUserLike = foundCampground.likes.some(function(like) {
// 			return like.equals(req.user._id);
// 		});

// 		if (foundUserLike) {
// 			// user already liked, removing like
// 			foundCampground.likes.pull(req.user._id);
// 		} else {
// 			// adding the new user like
// 			foundCampground.likes.push(req.user);
// 		}

// 		foundCampground.save(function(err) {
// 			if (err) {
// 				console.log(err);
// 				return res.redirect('/campgrounds');
// 			}
// 			return res.redirect('/campgrounds/' + foundCampground._id);
// 		});
// 	});
// });

//EDIT campground route
router.get('/:id/edit', middleware.checkCampgroundOwnership, function(req, res) {
	Campground.findById(req.params.id, function(err, foundCampground) {
		res.render('campgrounds/edit', { campground: foundCampground });
	});
});

//UPDATE campground route

router.put('/:id', middleware.checkCampgroundOwnership, function(req, res) {
	//find and update the correct campground
	Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground) {
		if (err) {
			res.redirect('/campgrounds');
		} else {
			res.redirect('/campgrounds/'); // + req.params.id);
		}
	});
	//redirect somewhere
});

// DESTROY CAMPGROUND ROUTE
router.delete('/:id', middleware.checkCampgroundOwnership, function(req, res) {
	Campground.findById(req.params.id, function(err, campground) {
		if (err) {
			res.redirect('/campgrounds');
		} else {
			// deletes all comments associated with the campground
			Comment.remove({ _id: { $in: campground.comments } }, function(err) {
				if (err) {
					console.log(err);
					return res.redirect('/campgrounds');
				}
				// deletes all reviews associated with the campground
				Review.remove({ _id: { $in: campground.reviews } }, function(err) {
					if (err) {
						console.log(err);
						return res.redirect('/campgrounds');
					}
					//  delete the campground
					campground.remove();
					req.flash('success', 'Ștergerea s-a realizat cu succes!');
					res.redirect('/campgrounds');
				});
			});
		}
	});
});

module.exports = router;
